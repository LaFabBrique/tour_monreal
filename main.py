#!/usr/bin/env python3
# -*- coding : utf-8 -*-

from PyQt5.QtCore import Qt, QObject, QUrl, QEvent, pyqtSlot, pyqtSignal
from PyQt5.QtWidgets import QApplication, QMainWindow, QDesktopWidget, QVBoxLayout, QFileDialog, QMessageBox
from PyQt5.QtGui import QPixmap, QImage, QPainter
from PyQt5.QtMultimedia import *
from PyQt5.QtMultimediaWidgets import *
from gui.ui_mainwindow import Ui_MainWindow

import json

import version
import constants

def getAppPath():
    # import sys
    # return sys.argv[0]
    import os
    dirname, filename = os.path.split(os.path.abspath(__file__))
    return dirname

def clickable(widget):

    class Filter(QObject):
    
        clicked = pyqtSignal('QLabel')
        
        def eventFilter(self, obj, event):
        
            if obj == widget:
                if event.type() == QEvent.MouseButtonRelease:
                    if obj.rect().contains(event.pos()):
                        self.clicked.emit(obj)
                        # The developer can opt for .emit(obj) to get the object within the slot.
                        return True
            
            return False
    
    filter = Filter(widget)
    widget.installEventFilter(filter)
    return filter.clicked
    

class MainWindowWrapper(QMainWindow, Ui_MainWindow):

    resized = pyqtSignal()

    def __init__(self, parent=None):
        super(MainWindowWrapper, self).__init__()
        self.setupUi(self)
        # self.center()
        self.showMaximized()
        self.setWindowTitle(version.APP_NAME + " - (Version : " \
            + version.VERSION_MAJOR + "." \
            + version.VERSION_MINOR + "." \
            + version.VERSION_PATCH + ")")

        self.videoFileName = ""
        self.childIndex = 0
        self.img = {}
        self.previousPictureStyle = ""
        self.questionNum = 0

        i = 0
        self.picturesPatri = []
        self.picturesPatri.append(self.imgPatri1)
        # TODO : Affecter le numéro d'indice à chaque image.
        self.imgPatri1.setNum(i)
        i += 1
        self.txtLog.appendPlainText("Image '" + self.imgPatri1.objectName() + "' ajoutée.")
        self.picturesPatri.append(self.imgPatri2)
        self.imgPatri2.setNum(i)
        i += 1
        self.txtLog.appendPlainText("Image '" + self.imgPatri2.objectName() + "' ajoutée.")
        self.picturesPatri.append(self.imgPatri3)
        self.imgPatri3.setNum(i)
        i += 1
        self.txtLog.appendPlainText("Image '" + self.imgPatri3.objectName() + "' ajoutée.")
        self.picturesPatri.append(self.imgPatri4)
        self.imgPatri4.setNum(i)
        i += 1
        self.txtLog.appendPlainText("Image '" + self.imgPatri4.objectName() + "' ajoutée.")
        self.picturesPatri.append(self.imgPatri5)
        self.imgPatri5.setNum(i)
        i += 1
        self.txtLog.appendPlainText("Image '" + self.imgPatri5.objectName() + "' ajoutée.")
        self.picturesPatri.append(self.imgPatri6)
        self.imgPatri6.setNum(i)
        i += 1
        self.txtLog.appendPlainText("Image '" + self.imgPatri6.objectName() + "' ajoutée.")
        self.txtLog.appendPlainText("Les objets 'image' pour les plantes partimoniales ont été ajoutés : " + str(len(self.picturesPatri)))

        self.picturesInva = []
        self.picturesInva.append(self.imgInva1)
        self.imgInva1.setNum(i)
        i += 1
        self.txtLog.appendPlainText("Image '" + self.imgInva1.objectName() + "' ajoutée.")
        self.picturesInva.append(self.imgInva2)
        self.imgInva2.setNum(i)
        i += 1
        self.txtLog.appendPlainText("Image '" + self.imgInva2.objectName() + "' ajoutée.")
        self.picturesInva.append(self.imgInva3)
        self.imgInva3.setNum(i)
        i += 1
        self.txtLog.appendPlainText("Image '" + self.imgInva3.objectName() + "' ajoutée.")
        self.picturesInva.append(self.imgInva4)
        self.imgInva4.setNum(i)
        i += 1
        self.txtLog.appendPlainText("Image '" + self.imgInva4.objectName() + "' ajoutée.")
        self.picturesInva.append(self.imgInva5)
        self.imgInva5.setNum(i)
        i += 1
        self.txtLog.appendPlainText("Image '" + self.imgInva5.objectName() + "' ajoutée.")
        self.picturesInva.append(self.imgInva6)
        self.imgInva6.setNum(i)
        i += 1
        self.txtLog.appendPlainText("Image '" + self.imgInva6.objectName() + "' ajoutée.")
        self.txtLog.appendPlainText("Les objets 'image' pour les plantes invasives ont été ajoutés : " + str(len(self.picturesInva)))
        
        self.pictures = self.picturesPatri + self.picturesInva

        self.showVideo()
        self.showImages()
        self.createConnections()
        self.loadMap()
        # self.displayLogo()


    def keyPressEvent(self, e):  
        if e.key() == Qt.Key_Escape:
            self.close()
        if e.key() == Qt.Key_F11:
            if self.isMaximized():
                self.showNormal()
            else:
                self.showMaximized()


    def center(self):
        """Centre la fenêtre principale."""

        # Récupère la dimension et la position de la fenêtre principale.
        qr = self.frameGeometry()

        # Récupère le point au centre de l'écran.
        cp = QDesktopWidget().availableGeometry().center()

        # Déplace le centre du rectangle au centre de l'écran.
        qr.moveCenter(cp)

        # Positionne la fenêtre sur le coin supérieur gauche du rectangle. 
        self.move(qr.topLeft())


    def showVideo(self):
        self.videoWidget = QVideoWidget()
        self.videoWidget.resize(300, 400)
        self.videoWidget.move(0, 0)

        # self.playList = QMediaPlaylist()

        # self.player = QMediaPlayer(None, QMediaPlayer.VideoSurface)
        self.player = QMediaPlayer()
        # self.player.setPlaylist(self.playList)
        self.player.setVideoOutput(self.videoWidget)
        
        layout = QVBoxLayout()
        layout.addWidget(self.videoWidget)
        self.frmVideo.setLayout(layout)


    def showImages(self):
        self.loadImages("patrimoniale", self.picturesPatri, constants.STYLE_BORDER_PATRI)
        self.loadImages("invasive", self.picturesInva, constants.STYLE_BORDER_INVA)
        self.frmPictures.adjustSize()
        self.selectPicture("reset")


    def loadMap(self):
        with open('assets/doc/carte-boutons.json') as jsonData:
            self.mapSpots = json.load(jsonData)
            print(self.mapSpots)


    def selectPicture(self, move):
        if self.previousPictureStyle != "":
            self.pictures[self.currentPicture].setStyleSheet(self.previousPictureStyle)

        if move == "reset":
            self.currentPicture = 0
        elif move == "left":
            if self.currentPicture % 3 > 0:
                self.currentPicture -= 1
        elif move == "right":
            if self.currentPicture % 3 < 2:
                self.currentPicture += 1
        elif move == "up":
            if self.currentPicture > 2:
                self.currentPicture -= 3
        elif move == "down":
            if self.currentPicture < len(self.pictures)-3:
                self.currentPicture += 3
        # elif move == "none":
        #     self.currentPicture =  

        self.previousPictureStyle = self.pictures[self.currentPicture].styleSheet()
        self.pictures[self.currentPicture].setStyleSheet(constants.STYLE_BORDER_SELECTED)


    def createConnections(self):
        self.resized.connect(self.resizeImages)


    def resizeEvent(self, event):
        self.resized.emit()
        return super(MainWindowWrapper, self).resizeEvent(event)  


    @pyqtSlot()
    def on_actionOpenVideo_triggered(self):
        self.txtLog.appendPlainText("Action : ouvrir un fichier vidéo.")
        
        options = QFileDialog.Options()
        # options |= QFileDialog.DontUseNativeDialog
        fileName, _ = QFileDialog.getOpenFileName(self, "Ouvrir un fichier vidéo...", "", "Tous les fichiers (*);;Vidéos (*.mp4 *.mkv)", options=options)
        if fileName:
            self.videoFileName = QUrl.fromLocalFile(fileName)
            self.player.setMedia(QMediaContent(self.videoFileName)) 
            # if self.playList.mediaCount() == 1:
            #     self.playList.removeMedia(1)
            # self.playList.addMedia(QMediaContent(self.videoFileName))
            self.txtLog.appendPlainText("Fichier choisi : " + str(self.videoFileName))


    @pyqtSlot()
    def on_actionPlayVideo_triggered(self):
        self.txtLog.appendPlainText("Action : jouer la vidéo.")
        if not self.videoFileName:
            # self.videoFileName = QUrl.fromLocalFile(getAppPath() + "/assets/mov/yoda.mkv")
            QMessageBox.critical(self, "ERREUR", "Vous devez d'abord choisir une vidéo.", QMessageBox.Ok)
        else:    
            self.player.play()


    @pyqtSlot()
    def on_actionStopVideo_triggered(self):
        self.txtLog.appendPlainText("Action : arrêter la vidéo.")
        self.player.stop()


    @pyqtSlot()
    def on_actionPauseVideo_triggered(self):
        self.txtLog.appendPlainText("Action : mettre en pause la vidéo.")
        self.player.pause()


    @pyqtSlot()
    def on_actionQuit_triggered(self):
        self.txtLog.appendPlainText("Action : quitter l'application.")
        QApplication.quit()
    

    @pyqtSlot()
    def on_actionRefreshPictures_triggered(self):
        self.txtLog.appendPlainText("Action : afficher les images.")
        self.showImages()


    @pyqtSlot()
    def on_actionSwapVideoImages_triggered(self):
        self.txtLog.appendPlainText("Action : intervertir la vidéo et les images.")
        children = self.splitterMedia.children()
        self.childIndex = 1 if self.childIndex == 0 else 0
        self.txtLog.appendPlainText("children : " + children[self.childIndex].objectName())
        self.splitterMedia.addWidget(children[self.childIndex])
        self.splitterMedia.show()


    @pyqtSlot()
    def on_btnNavLeft_clicked(self):
        self.txtLog.appendPlainText("Bouton : déplacement à gauche.")
        self.selectPicture("left")

    
    @pyqtSlot()
    def on_btnNavUp_clicked(self):
        self.txtLog.appendPlainText("Bouton : déplacement en haut.")
        self.selectPicture("up")

    
    @pyqtSlot()
    def on_btnNavDown_clicked(self):
        self.txtLog.appendPlainText("Bouton : déplacement en bas.")
        self.selectPicture("down")


    @pyqtSlot()
    def on_btnNavRight_clicked(self):
        self.txtLog.appendPlainText("Bouton : déplacement à droite.")
        self.selectPicture("right")


    @pyqtSlot()
    def on_btnNavSelect_clicked(self):
        self.txtLog.appendPlainText("Bouton : sélection de l'image.")
        self.showDescription(self.pictures[self.currentPicture])
        # TODO : Vérifier la réponse et MAJ carte
        self.questionNum += 1
        self.displayLogo()

    def showDescription(self, obj):
        self.txtLog.appendPlainText("Afficher la description : " + obj.toolTip())
        # TODO : Transmettre le numéro de l'image pour afficher la bordure de sélection.
        #self.selectPicture("")
        self.txtDescription.setSearchPaths(["."])
        self.txtDescription.setSource(QUrl(constants.PLANTS_DESCRIPTIONS_DIR \
            + obj.toolTip() + constants.PLANTS_DESCRIPTIONS_EXTENSION))


    def loadImages(self, prefix, imgArray, imgStyle):
        self.txtLog.appendPlainText("Charger les images : " + prefix)
        
        if prefix not in self.img:
            self.img[prefix] = []

        if len(self.img[prefix]) == 0:
            self.img[prefix] = self.findImages(prefix)
        
        i = 0
        for pic in imgArray:
            self.txtLog.appendPlainText("Image '" + pic.objectName()[-1] + "' chargée.")
            pixmap = QPixmap(self.img[prefix][i])
            pic.setMinimumSize(100,100)
            pic.setMaximumSize(400,400)
            # pic.setFixedSize(100, 100)
            pic.setToolTip(self.img[prefix][i][11:-4])
            pic.setPixmap(pixmap.scaled(pic.size()))
            pic.setStyleSheet(imgStyle)
            # pic.setPixmap(pixmap)
            clickable(pic).connect(self.showDescription)
            pic.show()
            i += 1


    def findImages(self, prefix):
        self.txtLog.appendPlainText("Trouver les images : " + prefix)
        import os
        pixmaps = []
        for file in os.listdir(constants.PLANTS_IMAGES_DIR):
            if (file.startswith(prefix) and file.endswith(constants.PLANTS_IMAGES_EXTENSION)):
                fileName = os.path.join(constants.PLANTS_IMAGES_DIR, file)
                self.txtLog.appendPlainText("Fichier '" + fileName + "' ajouté.")
                pixmaps.append(fileName)
        return pixmaps


    def resizeImages(self):
        self.txtLog.appendPlainText("Redimensionnement des images.")
        self.showImages()
        # self.txtLog.appendPlainText("Patrimoniales : " + str(len(self.picturesPatri)))
        # self.txtLog.appendPlainText("Invasives : " + str(len(self.picturesInva)))
        # for pic in self.picturesPatri:
        #     self.txtLog.appendPlainText("Taille : " + str(pic.size()))
        #     pic.setPixmap(pic.pixmap().scaled(pic.size()))
        #     pic.show()
        # for pic in self.picturesInva:
        #     self.txtLog.appendPlainText("Taille : " + str(pic.size()))
        #     pic.setPixmap(pic.pixmap().scaled(pic.size()))
        #     pic.show()
        # self.frmPictures.adjustSize()


    def displayLogo(self):
        print("[Logo] Affichage : " + str(self.questionNum))
        selectedSpot = {}
        print("Spots : ")
        print(self.mapSpots)
        for spot in self.mapSpots:
            print("Spot " + str(spot['num']) + " : ")
            print(spot)
            if spot['num'] == self.questionNum:
                selectedSpot = spot
                break
            
        if len(selectedSpot) == 0:
            print("[ERREUR] Aucun point correspondant sur la carte !")
            return

        print("spot : " + str(selectedSpot['num']))
        logo = QImage(":/ico/assets/ico/avs/Logo croix rouge carte.png").scaled(selectedSpot['w']+1,selectedSpot['h']+1)
        image = QImage(self.lblMap.pixmap())

        painter = QPainter()
        painter.begin(image)
        painter.drawImage(selectedSpot['x'], selectedSpot['y'], logo)        
        painter.end()
        self.lblMap.setPixmap(QPixmap.fromImage(image))


def main():
    import sys
    app = QApplication(sys.argv)   
    MainWindow = MainWindowWrapper()
    MainWindow.show()
    sys.exit(app.exec_())

if __name__ == '__main__':
    main()